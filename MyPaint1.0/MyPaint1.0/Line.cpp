#include "stdafx.h"
#include "Line.h"

CLine::CLine() {

}

CLine::CLine(int a, int b, int c, int d)
{
	beginX = a;
	beginY = b;
	endX = c;
	endY = d;
}

CLine::~CLine()
{
}

CShape* CLine::Create(int a, int b, int c, int d) {
	return new CLine(a, b, c, d);
}


void CLine::Draw(HDC hdc) {
	SelectObject(hdc, GetStockObject(NULL_BRUSH));
	MoveToEx(hdc, beginX, beginY, NULL);
	LineTo(hdc, endX, endY);
}
