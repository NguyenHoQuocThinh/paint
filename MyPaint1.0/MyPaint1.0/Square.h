#pragma once
#include "stdafx.h"
#include "Shape.h"
class CSquare: public CShape
{
private:
	int left;
	int right;
	int top;
	int bottom;
public:
	CSquare();
	CSquare(int a, int b, int c, int d);
	CShape* Create(int a, int b, int c, int d);
	void Draw(HDC hdc);
	~CSquare();
};

