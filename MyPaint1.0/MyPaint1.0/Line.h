#pragma once
#include "stdafx.h"
#include "Shape.h"


class CLine: public CShape
{
private:
	int beginX;
	int beginY;
	int endX;
	int endY;
public:
	CLine(int a, int b, int c, int d);
	CShape *Create(int a, int b, int c, int d);
	void Draw(HDC hdc);
	CLine();
	~CLine();
};

