#pragma once
#include "stdafx.h"
#include "Shape.h"

class CEllipse: public CShape
{
private:
	int left;
	int top;
	int right;
	int bottom;
public:
	CEllipse();
	CEllipse(int a, int b, int c, int d);
	CShape* Create(int a, int b, int c, int d);
	void Draw(HDC);
	~CEllipse();
};

