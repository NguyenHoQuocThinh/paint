﻿// MyPaint1.0.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "MyPaint1.0.h"
#include "Shape.h"
#include "Rectangle.h"
#include "Square.h"
#include "Line.h"
#include "Ellipse.h"
#include "Circle.h"
#include <vector>
#include <iostream>
#include <windows.h>
#include <commctrl.h>
#pragma comment(lib, "comctl32.lib")
using namespace std;

#include <windowsx.h>
#define MAX_LOADSTRING 100

#define DRAW_LINE 0;
#define DRAW_RECTANGLE 1;
#define DRAW_ELLIPSE 2;
#define DRAW_SQUARE 3;
#define DRAW_CIRCLE 4;

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

void DrawSquareAndCircle(HDC hdc, int left, int top, int right, int bottom, int mode);
CShape* SaveShape(int startX, int startY, int lastX, int lastY);
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MYPAINT10, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MYPAINT10));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYPAINT10));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MYPAINT10);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
bool isDrawing = false;
bool isShift = false;
int startX;
int startY;
int lastX;
int lastY;

vector<CShape*> shapes;
vector<CShape*> prototype;

int icurrent = 0;
HWND statusBar;

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	case WM_CREATE: {
		prototype.push_back(new CLine());
		prototype.push_back(new CRectangle());
		prototype.push_back(new CEllipse());
		prototype.push_back(new CSquare());
		prototype.push_back(new CCircle());
		statusBar = CreateWindow(STATUSCLASSNAME, NULL, WS_CHILD | WS_VISIBLE | SBARS_SIZEGRIP, 0, 0, 0, 0, hWnd, NULL, hInst, NULL);
		TCHAR *buffer = new TCHAR[50];
		wsprintf(buffer, L"Chế độ vẽ đường thẳng");
		SendMessage(statusBar, SB_SETTEXT, 0, (LPARAM)buffer);
	}break;
	case WM_MOUSEMOVE: {
		int x = GET_X_LPARAM(lParam);
		int y = GET_Y_LPARAM(lParam);
		WCHAR buffer[255];
		wsprintf(buffer, L"%d:%d", x, y);
		SetWindowText(hWnd, buffer);
		if (isDrawing) {
			lastX = x;
			lastY = y;
			InvalidateRect(hWnd, NULL, TRUE);
		}
	}break;
	case WM_LBUTTONDOWN: {
		int x = GET_X_LPARAM(lParam);
		int y = GET_Y_LPARAM(lParam);
		startX = x;
		startY = y;
		isDrawing = true;
	}break;
	case WM_LBUTTONUP: {
		CShape* shape = SaveShape(startX, startY, lastX, lastY);
		shapes.push_back(shape);
		isDrawing = false;
		InvalidateRect(hWnd, NULL, TRUE);
	}break;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
			case ID_MENU_LINE: {
				icurrent = DRAW_LINE;
				TCHAR *buffer = new TCHAR[50];
				wsprintf(buffer, L"Chế độ vẽ đường thẳng");
				SendMessage(statusBar, SB_SETTEXT, 0, (LPARAM)buffer);
			}break;
			case ID_MENU_RECTANGLE: {
				icurrent = DRAW_RECTANGLE;
				TCHAR *buffer = new TCHAR[50];
				wsprintf(buffer, L"Chế độ vẽ hình chữ nhật");
				SendMessage(statusBar, SB_SETTEXT, 0, (LPARAM)buffer);
			}break;
			case ID_MENU_ELLIPSE: {
				icurrent = DRAW_ELLIPSE;
				TCHAR *buffer = new TCHAR[50];
				wsprintf(buffer, L"Chế độ vẽ hình ê-líp");
				SendMessage(statusBar, SB_SETTEXT, 0, (LPARAM)buffer);
			}break;
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
	case WM_SIZE:
	{
		RECT main;
		GetWindowRect(hWnd, &main);
		int nStatusSize[] = { main.right };
		SendMessage(statusBar, SB_SETPARTS, 1, (LPARAM)&nStatusSize);
		MoveWindow(statusBar, 0, 0, main.right, main.bottom, TRUE);
	}break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);

			for (int i = 0; i < shapes.size(); i++) {
				shapes[i]->Draw(hdc);
			}

			if (isDrawing) {
				if (icurrent == 0) {
					MoveToEx(hdc, startX, startY, NULL);
					LineTo(hdc, lastX, lastY);
				}
				if(icurrent == 1)
					Rectangle(hdc, startX, startY, lastX, lastY);
				if(icurrent == 3)
					DrawSquareAndCircle(hdc, startX, startY, lastX, lastY, 1);
				if(icurrent == 2)
					Ellipse(hdc, startX, startY, lastX, lastY);
				if(icurrent == 4)
					DrawSquareAndCircle(hdc, startX, startY, lastX, lastY, 2);
			}
			
            EndPaint(hWnd, &ps);
        }
        break;
	case WM_KEYDOWN: {
		if (wParam == VK_SHIFT) {
			if (icurrent == 1) {
				icurrent = DRAW_SQUARE;
				TCHAR *buffer = new TCHAR[50];
				wsprintf(buffer, L"Chế độ vẽ hình hình vuông");
				SendMessage(statusBar, SB_SETTEXT, 0, (LPARAM)buffer);
			}
			if (icurrent == 2) {
				icurrent = DRAW_CIRCLE;
				TCHAR *buffer = new TCHAR[50];
				wsprintf(buffer, L"Chế độ vẽ hình hình tròn");
				SendMessage(statusBar, SB_SETTEXT, 0, (LPARAM)buffer);
			}
			isShift = true;
		}
	}break;
	case WM_KEYUP: {
		if (wParam == VK_SHIFT) {
			if (icurrent == 3) {
				icurrent = DRAW_RECTANGLE;
				TCHAR *buffer = new TCHAR[50];
				wsprintf(buffer, L"Chế độ vẽ hình hình chữ nhật");
				SendMessage(statusBar, SB_SETTEXT, 0, (LPARAM)buffer);
			}
			if (icurrent == 4) {
				icurrent = DRAW_ELLIPSE;
				TCHAR *buffer = new TCHAR[50];
				wsprintf(buffer, L"Chế độ vẽ hình hình ê-líp");
				SendMessage(statusBar, SB_SETTEXT, 0, (LPARAM)buffer);
			}
			isShift = false;
		}

	}break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}


void DrawSquareAndCircle(HDC hdc, int left, int top, int right, int bottom, int mode) {
	if (mode == 1) {
		if (left<right && top > bottom) {
			Rectangle(hdc, left, top, right - 5, top - (right - 5 - left));
		}
		if (left < right && top < bottom) {
			Rectangle(hdc, left, top, right - 5, top + (right - 5 - left));
		}
		if (left > right&&top > bottom) {
			Rectangle(hdc, left - 5, top, right, top - (left - right - 5));
		}
		if (left > right&& top < bottom) {
			Rectangle(hdc, left - 5, top, right, top + (left - right - 5));
		}
	}
	else {
		if (left<right && top > bottom) {
			Ellipse(hdc, left, top, right - 5, top - (right - 5 - left));
		}
		if (left < right && top < bottom) {
			Ellipse(hdc, left, top, right - 5, top + (right - 5 - left));
		}
		if (left > right&&top > bottom) {
			Ellipse(hdc, left - 5, top, right, top - (left - right - 5));
		}
		if (left > right&& top < bottom) {
			Ellipse(hdc, left - 5, top, right, top + (left - right - 5));
		}
	}
}

CShape* SaveShape(int startX, int startY, int lastX, int lastY) {
	if (isShift == false) {
		CShape* shape = prototype[icurrent]->Create(startX, startY, lastX, lastY);
		return shape;
	}
	else {
		if (startX < lastX&&startY > lastY) {
			CShape* shape = prototype[icurrent]->Create(startX, startY, lastX - 5, startY - (lastX - startX - 5));
			return shape;
		}
		if (startX < lastX&&startY < lastY) {
			CShape* shape = prototype[icurrent]->Create(startX, startY, lastX - 5, startY + (lastX - startX - 5));
			return shape;
		}
		if (startX > lastX&&startY > lastY) {
			CShape* shape = prototype[icurrent]->Create(startX - 5, startY, lastX, startY - (startX - lastX - 5));
			return shape;
		}
		if (startX > lastX&&startY < lastY) {
			CShape* shape = prototype[icurrent]->Create(startX - 5, startY, lastX, startY + (startX - lastX - 5));
			return shape;
		}
	}
	
}