//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MyPaint1.0.rc
//
#define IDC_MYICON                      2
#define IDD_MYPAINT10_DIALOG            102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_MYPAINT10                   107
#define IDI_SMALL                       108
#define IDC_MYPAINT10                   109
#define ID_TOOLBAR                      110
#define ID_BUTTON1                      111
#define IDR_MAINFRAME                   128
#define ID_MENU_LINE                    32776
#define ID_MENU_RECTANGLE               32777
#define ID_MENU_ELLIPSE                 32778
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32779
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           112
#endif
#endif
